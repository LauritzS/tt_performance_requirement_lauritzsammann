﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTPP
{
    public class Player : MonoBehaviour
    {
        public string playerCharacterName = "Captain Falcon";
        public int playerCharacterHealth = 100;
        public bool isPlayerCharacterAlive = true;
        public float playerCharacterSpeed = 17.32f;
        
        // Some more placeholder code :)
    }
}

